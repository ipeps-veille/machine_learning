# Machine Learning

## Problématique

L'intelligence artificielle est un vaste, très vaste sujet.

### Concept de base

Il y a quantité de techniques: *symboliques* (prolog), *connexionnistes*
(réseaux de neurones), **probabilistes** (k-mean (clusterisation), …), algos
génétiques, etc.

La plus appliquées des techniques connexionniste et celle des réseaux de
neurones.

La mise en œuvre de ces techniques fait toujours appel à la même suite
d'événements:

- La collection d'un immense dataset classifié.
- La division de ce dataset en deux parties:
    - Les données d'apprentissages sur lesquels le modèle va apprendre (ie
      modifier les poids des connexions)
    - Les données de validation du modèle pour vérifier que le modèle apprend
      bien le problème qui lui est soumis et non juste à représenter les
      données qu'on lui passe.

C'est ce qu'on appelle de l'apprentissage **supervisé** (les données sont
taggées auparavant par des humains). Le graal étant évidemment l'apprentissage
**non-supervisé** (celui que font les êtres vivants).

On va se concentrer sur les réseaux de neurones vu que c'est le domaine
émergeant du moment. En particulier le domaine de la classification d'image
via réseau de neurone.

## Le scénario de test

Problème classique du machine learning pour lequel un ensemble de données est
disponible: la classification de vêtements. Zalando ayant donné en accès libre
un ensemble d'images, on a tout ce qu'il faut pour commencer. De plus, il y a
plus de variété dans ce dataset que dans l'autre exemple classique de la
classification de nombres écrit à la main.

On essaiera ensuite de l'appliquer à des images venues du net.

## Recherche

Mots clés: *python machine learning frameworks*

## TensorFlow

Boite à outils qui provient de google
Intégré dans le cloud google
Doc nickel

## PyTorch

Boite à outil de FaceBook
AWS intègre pytorch
Plus récent
