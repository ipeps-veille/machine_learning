import os

import tensorflow as tf
from tensorflow import keras

import numpy as np
import matplotlib.pyplot as plt

from PIL import Image

fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = \
    fashion_mnist.load_data()

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

train_images = train_images / 255.0
test_images = test_images / 255.0


def show_array(array):
    plt.figure()
    plt.imshow(array)
    plt.colorbar()
    plt.grid(False)
    plt.show()


def create_model():
    if not os.path.exists('fashion.h5'):
        model = keras.Sequential([
            keras.layers.Flatten(input_shape=(28, 28)),
            keras.layers.Dense(128, activation='relu'),
            keras.layers.Dense(10, activation='softmax')
        ])

        model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])
        model.fit(train_images, train_labels, epochs=10)
        test_loss, test_acc = model.evaluate(
            test_images, test_labels, verbose=2)
        print('\nTest accuracy:', test_acc)
        model.save('fashion.h5')
    else:
        model = keras.models.load_model('fashion.h5')
    return model


model = create_model()


def check_image(path):
    img = Image.open(path).resize((28, 28)).convert('L')
    img.load()
    attire = np.asarray(img, dtype='int32')
    show_array(attire)
    prediction_batch = np.expand_dims(attire, 0)
    print(path, class_names[np.argmax(model.predict(prediction_batch))])


check_image('pullover.jpg')
check_image('coat.jpg')
check_image('sneakers.jpg')
check_image('dress.jpg')

check_image('pullover_dark.jpg')
check_image('coat_dark.jpg')
check_image('sneakers_dark.jpg')
check_image('dress_dark.jpg')
